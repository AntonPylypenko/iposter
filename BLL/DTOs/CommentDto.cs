﻿using System;
using DAL.Entities;

namespace BLL.DTOs
{
    public class CommentDto
    {
        /// <summary>
        /// topic field
        /// </summary>
        public Topic Topic { get; set; }

        /// <summary>
        /// date field
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// post field
        /// </summary>
        public string PostName { get; set; }
    }
}
