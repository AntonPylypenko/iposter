﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class PostDto
    {
        /// <summary>
        /// header field
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Image url field
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// TopicId field
        /// </summary>
        public int TopicId { get; set; }

        /// <summary>
        /// Text field
        /// </summary>
        public string Text { get; set; }
    }
}
