﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTOs
{
    public class PostShortDto
    {
        /// <summary>
        /// id of PostShortDto
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// header of PostShortDto
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// image url of PostShortDto
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Topic of of PostShortDto
        /// </summary>
        public string Topic { get; set; }

        /// <summary>
        /// Time of PostShortDto
        /// </summary>
        public DateTime Time { get; set; }
    }
}
