﻿using BLL.DTOs;
using BLL.Models;
using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ICommentService : ICrud<Comment, CommentDto>
    {
        Task<Comment> GetTheMostPopularComment();
    }
}
