﻿using BLL.DTOs;
using BLL.Models;
using DAL.Entities;
using System.Collections.Generic;

namespace BLL.Interfaces
{
    public interface IPostsService : ICrud<Post, PostShortDto>
    {
        IEnumerable<Post> GetByFilter(FilterSearchModel filterSearch);
    }
}
