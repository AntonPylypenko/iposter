﻿using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Interfaces
{
    public interface ITopicsService : ICrud<Topic>
    {
    }
}
