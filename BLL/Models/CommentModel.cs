﻿using DAL.Entities;
using System;
using System.Collections.Generic;

namespace BLL.Models
{
    public class CommentModel
    {
        public string UserName { get; set; }
        public DateTime Date { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
        //public List<int> CommentIds { get; set; }
        public List<Comment> Comments { get; set; }
    }
}
