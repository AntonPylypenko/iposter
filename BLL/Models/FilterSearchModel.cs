﻿namespace BLL.Models
{
    public class FilterSearchModel
    {
        public string UserName { get; set; }
        public int Year { get; set; }
    }
}
