﻿using DAL.Entities;
using System;
using System.Collections.Generic;

namespace BLL.Models
{
    public class PostModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Header { get; set; }
        public string ImageUrl { get; set; }
        public Topic Topic { get; set; }
        public int TopicId { get; set; }
        public DateTime Time { get; set; }
        public string Text { get; set; }
        public ICollection<int> CommentId { get; set; }
    }
}
