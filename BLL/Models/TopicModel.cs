﻿namespace BLL.Models
{
    public class TopicModel
    {
        public int Id { get; set; }
        public string TopicName { get; set; }
    }
}
