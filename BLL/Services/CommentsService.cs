﻿using AutoMapper;
using BLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Interfaces;
using DAL.Entities;
using System;
using BLL.DTOs;

namespace BLL.Services
{
    public class CommentsService : ICommentService
    {
        readonly IUnitOfWork _uow;
        //readonly Mapper _mapper;

        public CommentsService(IUnitOfWork unitOfWork/*, Mapper mapper*/)
        {
            _uow = unitOfWork;
          //_mapper = mapper;
        }

        public Task AddAsync(Comment model)
        {
            return _uow.CommentsRepository.AddAsync(/*_mapper.Map<CommentModel, Comment>(*/model/*)*/);
        }

        public Task DeleteByIdAsync(int modelId)
        {
            return _uow.CommentsRepository.DeleteByIdAsync(modelId);
        }

        public async Task<IEnumerable<CommentDto>> GetAll()
        {
           //List<CommentDto> commentDtoList = _mapper.Map<List<Comment>, List<CommentDto>>(await _uow.CommentsRepository.FindAll().ToListAsync());
           // return commentDtoList;
           throw new NotImplementedException();
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            //CommentModel commentModel = _mapper.Map<Comment, CommentModel>(await _uow.CommentsRepository.GetByIdAsync(id));
            Comment commentModel = await _uow.CommentsRepository.GetByIdAsync(id);
            return commentModel;
        }

        public Task<Comment> GetTheMostPopularComment()
        {
            // return Task.Run(() => GetTheMostPopularModel());
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Comment model)
        {
           // return Task.Run(() => { _uow.CommentsRepository.Update(_mapper.Map<CommentModel, Comment>(model)); });
            return Task.Run(() => _uow.CommentsRepository.Update(model));
        }
    }
}
