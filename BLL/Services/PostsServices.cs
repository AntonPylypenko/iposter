﻿using BLL.DTOs;
using BLL.Interfaces;
using BLL.Models;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class PostsService : IPostsService
    {
        readonly IUnitOfWork _uow;
        //readonly Mapper _mapper;

        public PostsService(IUnitOfWork unitOfWork/*, Mapper mapper*/)
        {
            _uow = unitOfWork;
            //_mapper = mapper;
        }

        public Task AddAsync(Post model)
        {
            return _uow.PostsRepository.AddAsync(/*_mapper.Map<PostModel, Post>(*/model/*)*/);
        }

        public Task DeleteByIdAsync(int modelId)
        {
            return _uow.PostsRepository.DeleteByIdAsync(modelId);
        }

        public async Task<IEnumerable<PostShortDto>> GetAll()
        {
            var posts = await _uow.PostsRepository.FindAll().ToListAsync();

            var postsDto = new List<PostShortDto>();

            foreach (var item in posts)
            {
                var postDto = new PostShortDto
                {
                    Id = item.Id,
                    Header = item.Header,
                    ImageUrl = item.ImageUrl,
                    Time = item.Time,
                    Topic = item.Topic.TopicName
                };
                postsDto.Add(postDto);
            }

            return postsDto;
        }

        public async Task<Post> GetByIdAsync(int id)
        {
            //PostModel postModel = _mapper.Map<Post, PostModel>(await _uow.PostsRepository.GetByIdAsync(id));
            Post postModel = await _uow.PostsRepository.GetByIdAsync(id);
            return postModel;
        }

        public Task<Post> GetTheMostPopularComment()
        {
            return Task.Run(() => GetTheMostPopularModel());
        }

        Post GetTheMostPopularModel() 
        {
            return null;// _mapper.Map<Post, PostModel>(_uow.PostsRepository.FindAll().OrderByDescending(item => item.Comments.Count).First());
        }

        public Task UpdateAsync(Post model)
        {
            return Task.Run(() => _uow.PostsRepository.Update(/*_mapper.Map<PostModel, Post>(*/model/*)); */));
        }

        public IEnumerable<Post> GetByFilter(FilterSearchModel filterSearch)
        {
            throw new System.NotImplementedException();
        }
    }
}
