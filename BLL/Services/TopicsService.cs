﻿using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TopicsService : ITopicsService
    {
        readonly IUnitOfWork _uow;
        //readonly Mapper _mapper;

        public TopicsService(IUnitOfWork unitOfWork/*, Mapper mapper*/)
        {
            _uow = unitOfWork;
            //_mapper = mapper;
        }

        public Task AddAsync(Topic model)
        {
            return _uow.TopicsRepository.AddAsync(/*_mapper.Map<PostModel, Post>(*/model/*)*/);
        }

        public Task DeleteByIdAsync(int modelId)
        {
            return _uow.TopicsRepository.DeleteByIdAsync(modelId);
        }

        public IEnumerable<Topic> GetAll()
        {
           return  _uow.TopicsRepository.FindAll();
        }

        public async Task<Topic> GetByIdAsync(int id)
        {
            //PostModel postModel = _mapper.Map<Post, PostModel>(await _uow.PostsRepository.GetByIdAsync(id));
            Topic postModel = await _uow.TopicsRepository.GetByIdAsync(id);
            return postModel;
        }

        public Task UpdateAsync(Topic model)
        {
            return Task.Run(() => _uow.TopicsRepository.Update(/*_mapper.Map<PostModel, Post>(*/model/*)); */));
        }
    }
}
