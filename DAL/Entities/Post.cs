﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    public class Post : BaseEntity
    {
        public string UserName { get; set; }
        public string Header { get; set; }
        public string ImageUrl { get; set; }
        public Topic Topic { get; set; }
        public int TopicId { get; set; }
        public DateTime Time { get; set; }
        public string Text { get; set; }
        public ICollection<Comment> Comments { get; set; }
        //public ICollection<int> CommentId{ get; set; }
    }
}
