﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Topic : BaseEntity
    {
        public string TopicName { get; set; }
    }
}
