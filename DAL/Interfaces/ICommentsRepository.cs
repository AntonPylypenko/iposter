﻿using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface ICommentsRepository : IRepository<Comment>
    {
        Task<List<Comment>> GetCommentsAsync();
    }
}
