﻿using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IPostsRepository : IRepository<Post>
    {
        Task<List<Post>> GetPostsAsync();
    }
}
