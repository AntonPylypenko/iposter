﻿using DAL.Entities;

namespace DAL.Interfaces
{
    public interface ITopicsRepository : IRepository<Topic>
    {
    }
}
