﻿using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IPostsRepository PostsRepository { get; }

        ICommentsRepository CommentsRepository { get; }

        ITopicsRepository TopicsRepository { get; }

        Task<int> SaveAsync();
    }
}
