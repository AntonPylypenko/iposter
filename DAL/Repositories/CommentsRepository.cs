﻿using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CommentsRepository :  ICommentsRepository
    {
        readonly StoreContext _context;
        public CommentsRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Comment entity)
        {
            await _context.Comments.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public void Delete(Comment entity)
        {
            _context.Comments.Remove(entity);
            _context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => _context.Comments.Remove(_context.Comments.Find(id)));
            await _context.SaveChangesAsync();
        }

        public IQueryable<Comment> FindAll()
        {
            return _context.Comments;
        }

        public IQueryable<Comment> FindAllWithDetails()
        {
            return _context.Comments
                .Include(c => c.UserName)
                .Include(c => c.Likes)
                .Include(c => c.Text)
                .Include(c => c.Comments);
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _context.Comments.FindAsync(id);
        }

        public async Task<Comment> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => FindAllWithDetails().FirstOrDefault(r => r.Id == id));
        }

        public async Task<List<Comment>> GetCommentsAsync()
        {
            return await Task.Run(() => FindAll().ToList());
        }

        public void Update(Comment entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
