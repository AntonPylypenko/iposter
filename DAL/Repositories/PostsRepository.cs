﻿using DAL.Entities;
using DAL.Interfaces;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class PostsRepository : IPostsRepository
    {
        readonly StoreContext _context;

        public PostsRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Post entity)
        {
            await _context.Posts.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public void Delete(Post entity)
        {
            _context.Posts.Remove(entity);
            _context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => _context.Posts.Remove(_context.Posts.Find(id)));
            await _context.SaveChangesAsync();
        }

        public IQueryable<Post> FindAll()
        {
            return _context.Posts.Include(p=>p.Topic);
        }

        public IQueryable<Post> FindAllWithDetails()
        {
            return _context.Posts
                .Include(c => c.Topic)
                .Include(c => c.Time);
        }

        public async Task<Post> GetByIdAsync(int id)
        {
            return await _context.Posts.FindAsync(id);
        }

        public async Task<Post> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => FindAllWithDetails().FirstOrDefault(r => r.Id == id));
        }

        public async Task<List<Post>> GetPostsAsync()
        {
            return await Task.Run(() => FindAll().ToList());
        }

        public void Update(Post entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}
