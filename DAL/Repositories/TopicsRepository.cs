﻿using DAL.Data;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TopicsRepository : ITopicsRepository
    {
        readonly StoreContext _context;

        public TopicsRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Topic entity)
        {
            await _context.Topics.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public void Delete(Topic entity)
        {
            _context.Topics.Remove(entity);
            _context.SaveChanges();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await Task.Run(() => _context.Topics.Remove(_context.Topics.Find(id)));
            await _context.SaveChangesAsync();
        }

        public IQueryable<Topic> FindAll()
        {
            return _context.Topics;
        }

        public IQueryable<Topic> FindAllWithDetails()
        {
            return _context.Topics;
        }

        public async Task<Topic> GetByIdAsync(int id)
        {
            return await _context.Topics.FindAsync(id);
        }

        public async Task<Topic> GetByIdWithDetailsAsync(int id)
        {
            return await Task.Run(() => FindAllWithDetails().FirstOrDefault(r => r.Id == id));
        }

        public async Task<List<Topic>> GetPostsAsync()
        {
            return await Task.Run(() => FindAll().ToList());
        }

        public void Update(Topic entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
    }
}

