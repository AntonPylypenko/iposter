﻿using DAL.Interfaces;
using DAL.Data;
using DAL.Repositories;
using System.Threading.Tasks;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly StoreContext _context;

        public UnitOfWork(StoreContext context)
        {
            _context = context;
        }

        private IPostsRepository postsRepository;
        private ICommentsRepository commentsRepository;
        private ITopicsRepository topicsRepository;

        public IPostsRepository PostsRepository
        {
            get
            {
                if (postsRepository == null)
                    postsRepository = new PostsRepository(_context);
                return postsRepository;
            }
        }

        public ICommentsRepository CommentsRepository
        {
            get
            {
                if (commentsRepository == null)
                    commentsRepository = new CommentsRepository(_context);
                return commentsRepository;
            }
        }

        public ITopicsRepository TopicsRepository
        {
            get
            {
                if (topicsRepository == null)
                    topicsRepository = new TopicsRepository(_context);
                return topicsRepository;
            }
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
