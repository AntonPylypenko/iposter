﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using BLL.Interfaces;
using BLL.DTOs;
using DAL.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using BLL.Services;

namespace PL.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PostsController : ControllerBase
    {
        IPostsService postService;
        ITopicsService topicService;
        private readonly StoreContext context;

        public PostsController(IPostsService postService, ITopicsService topicService, StoreContext context)
        {
            this.postService = postService;
            this.topicService = topicService;
            this.context = context;
        }

        [HttpGet]
        public async Task<ActionResult<List<PostShortDto>>> GetPosts()
        {
            var posts = await postService.GetAll();
            //var posts = await context.Posts.Include(u => u.Topic).ToListAsync();
            //var posts = await _repository.GetPostsAsync();
            return Ok(posts);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Post>> GetPostById(int id)
        {
            return Ok(await postService.GetByIdAsync(id));
        }


        //[HttpPost]
        //public async Task<ActionResult> CreatePost(PostDto postDto)
        //{
        //    var topic = await context.Topics.Where(t => t.Id == postDto.TopicId).FirstOrDefaultAsync();
        //    if (topic == null)
        //    {
        //        return BadRequest();
        //    }
        //    var post = new Post
        //    {
        //        Header = postDto.Header,
        //        Topic = topic,
        //        Time = DateTime.Now,
        //        Text = postDto.Text,
        //        ImageUrl = postDto.ImageUrl,
        //        UserName = "Anonimus"
        //    };
        //    await postService.AddAsync(post);
        //    return Ok();
        //}


        [HttpGet("topics")]
        public ActionResult<List<Topic>> GetAllTopics()
        {
            var topics = topicService.GetAll().ToList();
            //var posts = await context.Posts.Include(u => u.Topic).ToListAsync();
            //var posts = await _repository.GetPostsAsync();
            return Ok(topics);
        }



        [HttpPost]
        public async Task<ActionResult> CreatePost()
        {
            var topic1 = new Topic()
            {
                TopicName = "Travels"
            };

            var topic2 = new Topic()
            {
                TopicName = "Politics"
            };


            var topic3 = new Topic()
            {
                TopicName = "Weather"
            };


            var topic4 = new Topic()
            {
                TopicName = "Technics"
            };


            var topic5 = new Topic()
            {
                TopicName = "Future"
            };

            await topicService.AddAsync(topic1);
            await topicService.AddAsync(topic2);
            await topicService.AddAsync(topic3);
            await topicService.AddAsync(topic4);
            await topicService.AddAsync(topic5);

            return Ok();

            //    var post1 = new Post()
            //    {
            //        Header = "Michigan",
            //        ImageUrl = "https://i.pinimg.com/736x/50/df/34/50df34b9e93f30269853b96b09c37e3b.jpg",
            //        Text = "The area was first occupied by a succession of Native American tribes over thousands of years. Inhabited by Natives, Métis, and French explorers in the 17th century, it was claimed as part of New France colony. After France's defeat in the French and Indian War in 1762, the region came under British rule. Britain ceded the territory to the newly independent United States after Britain's defeat in the American Revolutionary War. The area was part of the larger Northwest Territory until 1800, when western Michigan became part of the Indiana Territory. Michigan Territory was formed in 1805, but some of the northern border with Canada was not agreed upon until after the War of 1812. Michigan was admitted into the Union in 1837 as the 26th state, a free one. It soon became an important center of industry and trade in the Great Lakes region and a popular émigré destination in the late 19th and early 20th centuries; immigration from many European countries to Michigan was also the busiest at that time, especially for those who emigrated from Finland, Macedonia and the Netherlands.",
            //        Time = DateTime.Now,
            //        Topic = topic,
            //        UserName = "Den"
            //    };

            //    var post2 = new Post()
            //    {
            //        Header = "Colorado",
            //        ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Colorado_National_Monument_%284939640266%29.jpg/1280px-Colorado_National_Monument_%284939640266%29.jpg",
            //        Text = "Denver is the capital and most populous city in Colorado. Residents of the state are known as Coloradans, although the antiquated term " +
            //        "\"Coloradoan\" is occasionally used.[13][14] Colorado is a comparatively wealthy state, ranking 8th in household income in 2016,[15] and 11th in per capita income in 2010.[16] Major parts of the economy include government and defense, mining, agriculture, tourism, and increasingly other kinds of manufacturing. With increasing temperatures and decreasing water availability, Colorado's agriculture, forestry and tourism economies are expected to be heavily affected by climate change.",
            //        Time = DateTime.Now,
            //        Topic = topic,
            //        UserName = "Toha"
            //    };

            //    var post3 = new Post()
            //    {
            //        Header = "Texas",
            //        ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/d/df/TexasStateCapitol-2010-01.JPG/1280px-TexasStateCapitol-2010-01.JPG",
            //        Text = "Houston is the most populous city in Texas and the fourth largest in the U.S., while San Antonio is the second-most populous in the state and seventh largest in the U.S. Dallas–Fort Worth and Greater Houston are the fourth and fifth largest metropolitan statistical areas in the country, respectively. Other major cities include Austin, the second-most populous state capital in the U.S., and El Paso. Texas is nicknamed the \"Lone Star State\" for its former status as an independent republic, and as a reminder of the state's struggle for independence from Mexico. The \"Lone Star\" can be found on the Texas state flag and on the Texas state seal.[11] The origin of Texas's name is from the word táyshaʼ, which means \"friends\" in the Caddo language.",
            //        Time = DateTime.Now,
            //        Topic = topic,
            //        UserName = "Alex"
            //    };

            //    var post4 = new Post()
            //    {
            //        Header = "Turkey",
            //        ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Ephesus_Celsus_Library_Fa%C3%A7ade.jpg/1280px-Ephesus_Celsus_Library_Fa%C3%A7ade.jpg",
            //        Text = "Turkey (Turkish: Türkiye [ˈtyɾcije]), officially the Republic of Turkey (Turkish: Türkiye Cumhuriyeti [ˈtyɾcije dʒumˈhuːɾijeti] (About this soundlisten)), is a transcontinental country located mainly on the Anatolian Peninsula in Western Asia, with a smaller portion on the Balkan Peninsula in Southeastern Europe. Turkey is bordered on its northwest by Greece and Bulgaria; north by the Black Sea; northeast by Georgia; east by Armenia, Azerbaijan, and Iran; southeast by Iraq; south by Syria and the Mediterranean Sea; and west by the Aegean Sea. Approximately 70 to 80 percent of the country's citizens are Turkish[10][11]. Istanbul, which straddles Europe and Asia, is the country's largest city, while Ankara is the capital.",
            //        Time = DateTime.Now,
            //        Topic = topic,
            //        UserName = "Alex"
            //    };

            //    var post5 = new Post()
            //    {
            //        Header = "Japan",
            //        ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Japan_%28orthographic_projection%29.svg/800px-Japan_%28orthographic_projection%29.svg.png",
            //        Text = "Japan has been inhabited since the Upper Paleolithic period (30,000 BC), though the first mentions of the archipelago appear in Chinese chronicles from the 1st century AD. Between the 4th and 9th centuries, the kingdoms of Japan became unified under an emperor and his imperial court based in Heian-kyō. Beginning in the 12th century, however, political power was held by a series of military dictators (shōgun) and feudal lords (daimyō), and enforced by a class of warrior nobility (samurai). After a century-long period of civil war, the country was reunified in 1603 under the Tokugawa shogunate, which enacted an isolationist foreign policy. In 1854, a United States fleet forced Japan to open trade to the West, which led to the end of the shogunate and the restoration of imperial power in 1868. In the Meiji period, the Empire of Japan adopted a Western-styled constitution and pursued a program of industrialization and modernization. In 1937, Japan invaded China; in 1941, it entered World War II as an Axis power. After suffering defeat in the Pacific War and two atomic bombings, Japan surrendered in 1945 and came under a seven-year Allied occupation, during which it adopted a new constitution. Since 1947, Japan has maintained a unitary parliamentary constitutional monarchy with a bicameral legislature, the National Diet.",
            //        Time = DateTime.Now,
            //        Topic = topic,
            //        UserName = "Anton"
            //    };

            //    //await service.AddAsync(post1);
            //    //await service.AddAsync(post2);
            //    //await service.AddAsync(post3);
            //    //await service.AddAsync(post4);
            //    //await service.AddAsync(post5);
            //    return Ok();
        }
    }
}