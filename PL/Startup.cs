using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using DAL.Data;
using BLL.Interfaces;
using BLL.Services;
using DAL.Interfaces;
using DAL;
using AutoMapper;

namespace PL
{
    public class Startup
    {
        private readonly IConfiguration _config;
        public Startup(IConfiguration config)
        {
            _config = config;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddScoped<IPostRepository, PostRepository>();
            services.AddControllers();
            services.AddDbContext<StoreContext>(
                x => x.UseSqlServer(_config.GetConnectionString("DefaultConnection"))
            );
            services.AddScoped<IPostsService, PostsService>();
            services.AddScoped<ITopicsService, TopicsService>();
            services.AddScoped<ICommentService, CommentsService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddCors();
            //services.AddScoped<IMapper, Mapper>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
