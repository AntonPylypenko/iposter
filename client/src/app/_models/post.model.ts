export class PostModel {
    public id: number;
    public header: string;
    public imageUrl: string;
    public topic: string;
    public topicId: number;
    public time: Date;
    public text: string;
    public comments: string;
    constructor() {}
}