export class PostShortModel {
    constructor(
    public id: number,
    public header: string,
    public imageUrl: string,
    public topic: string,
    public time: Date
    ) {}
}