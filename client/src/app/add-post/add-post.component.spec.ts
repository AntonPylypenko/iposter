/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Add_postComponent } from './add-post.component';

describe('Add_postComponent', () => {
  let component: Add_postComponent;
  let fixture: ComponentFixture<Add_postComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Add_postComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Add_postComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
