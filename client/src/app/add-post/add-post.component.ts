import { Component, OnInit } from '@angular/core';
import { StoryService } from '../services/story.service';
import { PostModel } from '../_models/post.model';
import { TopicModel } from '../_models/topic.model';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class Add_postComponent implements OnInit {
  post: PostModel = new PostModel();
  topics: TopicModel[];
  constructor(private storyService: StoryService) { }

  ngOnInit() {
    this.storyService.getTopics().subscribe(responce => {
      this.topics = responce;
    })
  }

onAddPost(){
  this.post.topicId = +this.post.topicId;
  this.storyService.addPost(this.post).subscribe();
}



}
