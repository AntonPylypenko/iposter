import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { Add_postComponent } from './add-post/add-post.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { SigninComponent } from './signin/signin.component';
import { StoryItemComponent } from './story-item/story-item.component';
import { StoryComponent } from './story/story.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: RegistrationComponent },
  { path: 'aboutus', component: AboutusComponent },
  { path: 'story/:id', component: StoryItemComponent },
  { path: 'addpost', component: Add_postComponent },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }

  //{ path: 'about-us', component: About },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
