import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { StoryComponent } from './story/story.component';
import { StoryItemComponent } from './story-item/story-item.component';
import { CommentFormComponent } from './comment-form/comment-form.component';
import { LeaveCommentComponent } from './leave-comment/leave-comment.component';
import { CommentsComponent } from './comments/comments.component';
import { CommentReplyComponent } from './comment-reply/comment-reply.component';
import { FooterComponent } from './footer/footer.component';
import { RegistrationComponent } from './registration/registration.component';
import { SigninComponent } from './signin/signin.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { Add_postComponent } from './add-post/add-post.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [								
    AppComponent,
    HeaderComponent,
    HomeComponent,
    StoryComponent,
    StoryItemComponent,
    CommentFormComponent,
    LeaveCommentComponent,
      CommentsComponent,
      CommentReplyComponent,
      FooterComponent,
      RegistrationComponent,
      SigninComponent,
      AboutusComponent,
      Add_postComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
