import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  public isReply: boolean = false;
  public isAddedComment: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.isReply = !this.isReply;
    if(!this.isReply) {
      this.isAddedComment = true;
    }
  }
}
