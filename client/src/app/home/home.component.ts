import { Component, OnInit } from '@angular/core';
import { StoryService } from '../services/story.service';
import { PostShortModel } from '../_models/postShort.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public posts: PostShortModel[] = [];

  constructor(public storyService: StoryService) { }

  ngOnInit() {
    this.getPosts();
  }

  public getPosts() {
    this.storyService.getPosts().subscribe(responce => {
      this.posts = responce;
    })
  }

  
}
