import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PostModel } from '../_models/post.model';
import { PostShortModel } from '../_models/postShort.model';
import { TopicModel } from '../_models/topic.model';

@Injectable({
  providedIn: 'root'
})
  export class StoryService {
    baseUrl = 'https://localhost:44344/api/';

  constructor(private http: HttpClient) { }

  public getPosts() {
    return this.http.get<PostShortModel[]>(this.baseUrl + 'posts');
  }

  public getPostsById(id: number) {
    return this.http.get<PostModel>(this.baseUrl + 'posts/' + id);
  }

  public addPost(post: PostModel) {
    return this.http.post<PostModel>(this.baseUrl + 'posts', post);
  }

  public getTopics() {
    return this.http.get<TopicModel[]>(this.baseUrl + 'posts/topics');
  }
}
