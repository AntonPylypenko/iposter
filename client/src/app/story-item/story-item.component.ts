import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StoryService } from '../services/story.service';
import { PostModel } from '../_models/post.model';

@Component({
  selector: 'app-story-item',
  templateUrl: './story-item.component.html',
  styleUrls: ['./story-item.component.scss']
})
export class StoryItemComponent implements OnInit {
  private id: number;
  public postItem: PostModel;
  constructor(private storyService: StoryService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id']; 
      this.getPosts(this.id).subscribe(responce => {
        this.postItem = responce;
      })
    });
  }

  public getPosts(id: number) {
    return this.storyService.getPostsById(id);
  }


}
