import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostShortModel } from '../_models/postShort.model';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  @Input() public post: PostShortModel;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  btnClick() {
    this.router.navigateByUrl('/story/'+ this.post.id);
  }

}
